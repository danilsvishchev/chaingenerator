package com.example.ccgrammar.service;

import com.example.ccgrammar.component.CcGenerator;
import com.example.ccgrammar.component.IGrammarFileUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

@Service
public class GrammarService {
    private final IGrammarFileUtils iGrammarFileUtils;

    public GrammarService(IGrammarFileUtils iGrammarFileUtils) {
        this.iGrammarFileUtils = iGrammarFileUtils;
    }

    public List<String> generateSymbolChain(int numberOfChains) throws IOException {
        Map<String, List<String>> map = iGrammarFileUtils.readGrammarFromFile();

        CcGenerator ccGenerator = new CcGenerator(map);

        List<String> chains = ccGenerator.generateRudimentaryOutputWithFixNumber(numberOfChains);

        iGrammarFileUtils.writeSymbolsToFile(chains);

        return chains;
    }
}
