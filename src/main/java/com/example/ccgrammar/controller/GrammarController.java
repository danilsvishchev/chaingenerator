package com.example.ccgrammar.controller;

import com.example.ccgrammar.service.GrammarService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/grammar")
public class GrammarController {
    private final GrammarService grammarService;

    public GrammarController(GrammarService grammarService) {
        this.grammarService = grammarService;
    }

    @GetMapping("/generateChains")
    public List<String> generateSymbolChain (@RequestParam(name = "number", defaultValue = "5") int number) throws IOException {
        return grammarService.generateSymbolChain(number);
    }
}
