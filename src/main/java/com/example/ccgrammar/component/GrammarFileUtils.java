package com.example.ccgrammar.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.*;

/**
 * Утилитный класс чтение и записи в файл
 */
@Component
public class GrammarFileUtils implements IGrammarFileUtils {
    private static final Logger logger = LoggerFactory.getLogger(GrammarFileUtils.class);

    private final String source;
    private final String destination;

    public GrammarFileUtils(@Value("${source.file.path:text.txt}") String source,
                            @Value("${destination.file.path:text.txt}") String destination) {
        this.source = source;
        this.destination = destination;
    }

    @Override
    public Map<String, List<String>> readGrammarFromFile() throws IOException {
        Map<String, List<String>> map = new HashMap<>();
        Files.lines(Paths.get(source), StandardCharsets.UTF_8).forEach(line -> {
            List<String> value = new ArrayList<>();
            StringBuilder builder = new StringBuilder();
            for (int i = 3; i < line.length(); i++) {
                char symbol = line.charAt(i);
                if (symbol == '|') {
                    value.add(builder.toString().trim());
                    builder.delete(0, builder.length());
                } else {
                    builder.append(symbol);
                }
            }
            value.add(builder.toString().trim());
            map.put(line.substring(0, 1), value);
        });
        logger.info("Successfully read grammar from file {}", source);
        return map;
    }

    @Override
    public void writeSymbolsToFile(List<String> chains) throws IOException {
        Files.write(Path.of(new File(destination).getAbsolutePath()), (chains.toString() + "\n").getBytes(), StandardOpenOption.APPEND);
        logger.info("Successfully write chains to file {}", destination);
    }

}
