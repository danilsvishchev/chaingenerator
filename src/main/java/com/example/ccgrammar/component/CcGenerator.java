package com.example.ccgrammar.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Класс генерации символьных последовательностей
 */
public class CcGenerator {
    private static final Logger logger = LoggerFactory.getLogger(CcGenerator.class);

    private final Map<String, List<String>> ccGrammar;

    private final List<String> nonTerminals;

    public CcGenerator(Map<String, List<String>> ccGrammar) {
        this.ccGrammar = ccGrammar;
        this.nonTerminals = new ArrayList<>(ccGrammar.keySet());
    }

    public List<String> generateRudimentaryOutputWithFixNumber(int number) {
        List<String> chains = new ArrayList<>(number);
        while (number > 0) {
            chains.add(generateRudimentaryOutput());
            number--;
        }
        return chains;
    }

    public String generateRudimentaryOutput() {
        logger.info("Start generate chain");
        if (ccGrammar.containsKey("S")) {
            String randomRule = getRandomRule("S");
            logger.info("Start rule {}", randomRule);
            return recursive(randomRule);
        }
        throw new RuntimeException("Grammar not contains start symbol \"S\"");
    }

    private String recursive(String randomRule) {
        String result = "";
        StringBuilder builder = new StringBuilder(randomRule);
        boolean isTerminalString = true;

        for (String nonTerminal : nonTerminals) {
            if (randomRule.contains(nonTerminal)) {
                isTerminalString = false;
                int index = randomRule.indexOf(nonTerminal);
                String replaceRule = getRandomRule(nonTerminal);
                builder.replace(index, index + 1, replaceRule);
                logger.info("Replace non terminal {} to rule {}", nonTerminal, replaceRule);
                return recursive(builder.toString());
            }
        }

        return isTerminalString ? randomRule : result;
    }

    private String getRandomRule(String symbol) {
        if (ccGrammar.containsKey(symbol)) {
            Random rnd = new Random();
            List<String> rules = ccGrammar.get(symbol);
            int i = rnd.nextInt(rules.size());
            return rules.get(i);
        }
        throw new RuntimeException("Grammar not contains symbol");
    }

    public Map<String, List<String>> getCcGrammar() {
        return ccGrammar;
    }

}
