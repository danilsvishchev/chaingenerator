package com.example.ccgrammar.component;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface IGrammarFileUtils {
    Map<String, List<String>> readGrammarFromFile() throws IOException;

    void writeSymbolsToFile(List<String> chains) throws IOException;
}
