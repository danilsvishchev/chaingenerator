package com.example.ccgrammar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CcGrammarApplication {

    public static void main(String[] args) {
        SpringApplication.run(CcGrammarApplication.class, args);
    }

}
